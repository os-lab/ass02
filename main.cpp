#include <fcntl.h>
#include "bits/stdc++.h"
#include "sys/stat.h"
#include "unistd.h"
#include "sys/wait.h"
using namespace std;

#define checkExec(cmd, err) if((cmd) == -1)\
 {\
    perror(err);\
    exit(0);\
}

void errorInFork() {
    printf(" +++ The fork process failed! Exiting now ...\n");
    exit(1);
}

void strVec2chrDarr (vector<string> strVec, char *** chrDarr) {
    int iter, size = strVec.size();
    (*chrDarr) = (char **) malloc((size + 1) * sizeof(char*));
    for (iter = 0; iter < size; iter ++) {
        (*chrDarr)[iter] = (char *) malloc ((strVec[iter].size() + 1) * sizeof(char));
        strcpy((*chrDarr)[iter], strVec[iter].c_str());
    }
    (*chrDarr)[iter] = (char*) NULL;
}

vector<string> splitStr (string str) {
    vector <string> strCompo;
    istringstream iss(str);
    copy(istream_iterator<string>(iss), istream_iterator<string>(), back_inserter(strCompo));
    return strCompo;
}

string trim(const string& str) {
    size_t first = str.find_first_not_of(' ');
    if (string::npos == first) {
        return str;
    }
    size_t last = str.find_last_not_of(' ');
    return str.substr(first, (last - first + 1));
}

template <typename T>
void printVec(vector <T> vec) {
    for (int iter = 0; iter < vec.size(); iter++)
        cout << vec.at(iter) << " ";
    cout << "\n";
}

int getRedirPos (string cmd, char redirDir) {
    int pos, size = cmd.size(), redirInDQ = -1, redirInSQ = -1;
    for (pos = 0; pos < size; pos ++) {
        if (cmd[pos] == '"')
            redirInDQ *= -1;
        if (cmd[pos] == '\'')
            redirInSQ *= -1;
        if (cmd[pos] == redirDir && redirInDQ + redirInSQ == -2)
            return pos;
    }
}
vector<int> getPipePos (string cmd) {
    vector <int> posVec;
    posVec.push_back(-1);
    int pos, size = cmd.size(), redirInDQ = -1, redirInSQ = -1;
    for (pos = 0; pos < size; pos ++) {
        if (cmd[pos] == '"')
            redirInDQ *= -1;
        if (cmd[pos] == '\'')
            redirInSQ *= -1;
        if (cmd[pos] == '|' && redirInDQ + redirInSQ == -2)
            posVec.push_back(pos);
    }
    return posVec;
}

void execInternal (string cmd) {
    vector <string> cmdAndArgs = splitStr(cmd);
    int iter, size = cmdAndArgs.size(), res;
    mode_t mask;
    if (cmdAndArgs[0] == "chdir") {
        checkExec(chdir(cmdAndArgs[1].c_str()), cmdAndArgs[0].c_str());
    }
    else if (cmdAndArgs[0] == "mkdir") {
        for (iter = 1; iter < size; iter ++) {
            checkExec(mkdir(cmdAndArgs[iter].c_str(), 0777), cmdAndArgs[0].c_str());
        }
    }
    else if (cmdAndArgs[0] == "rmdir") {
        for (iter = 1; iter < size; iter ++) {
            checkExec(rmdir(cmdAndArgs[iter].c_str()), cmdAndArgs[0].c_str());
        }
    }
}

void execExternal (string cmd, int redirMode) {
    int status, pos, fileDesc;
    char **cmdAndArgsArr;
    vector <string> cmdAndArgs;
    pid_t x = fork();
    if (x == 0) {
        if (redirMode == 0 || redirMode == 3)
            cmdAndArgs = splitStr(cmd);
        else {
            if (redirMode == 1) {
                pos = getRedirPos(cmd, '<');
                fileDesc = open(trim(cmd.substr(pos + 1)).c_str(), O_RDONLY);
                fclose(stdin);
            } else if (redirMode == 2){
                pos = getRedirPos(cmd, '>');
                fileDesc = open(trim(cmd.substr(pos + 1)).c_str(), O_CREAT | O_RDWR, 0777);
                fclose(stdout);
            }
            cmdAndArgs = splitStr(cmd.substr(0, pos));
            dup(fileDesc);
        }
        strVec2chrDarr (cmdAndArgs, &cmdAndArgsArr);
        execvp(*cmdAndArgsArr, cmdAndArgsArr);
        if (redirMode > 0)
            close(fileDesc);
        exit(0);
    } else if (x > 0) {
        if (redirMode != 3)
            while (wait(&status) != x);
        sleep(1);
    } else {
        errorInFork();
    }
}
int forkOne (int in, int out, char **cmd) {
    pid_t x;
    if ((x = fork()) == 0) {
        if (in != 0) {
            dup2(in, 0);
            close(in);
        }
        if (out != 1) {
            dup2(out, 1);
            close(out);
        }
        return execvp (*cmd, cmd);
    }
    if (x < 0)
        errorInFork();
    return x;
}
void pipeEmAll(string cmd) {
    vector<int> pipePos = getPipePos(cmd);
    int iter, size = pipePos.size(), reusePipe[2], in = 0, status, saved_stdin, saved_stdout;
    vector<string> cmdAndArgs;
    char **cmdAndArgsArr;
    pid_t x;
    saved_stdin = dup(0);
    saved_stdout = dup(1);

    for (iter = 0; iter < size - 1; iter ++) {
        pipe(reusePipe);
        cmdAndArgs = splitStr(cmd.substr(pipePos[iter] + 1, pipePos[iter + 1] - pipePos[iter] - 1));
        strVec2chrDarr (cmdAndArgs, &cmdAndArgsArr);
        forkOne (in, reusePipe[1], cmdAndArgsArr);
        close (reusePipe[1]);
        in = reusePipe[0];
    }
    if (in != 0)
        dup2 (in, 0);
    cmdAndArgs = splitStr(cmd.substr(pipePos[iter] + 1));
    strVec2chrDarr (cmdAndArgs, &cmdAndArgsArr);
    x = fork();
    if (x == 0) {
        execvp(*cmdAndArgsArr, cmdAndArgsArr);
        printf("\n");
        exit(0);
    } else {
        while (wait(&status) != x);
        dup2(saved_stdin, 0);
        dup2(saved_stdout, 1);
    }
}

int main() {
    char opt;
    int iter;
    string cmd;
    vector<string> cmdAndArgs;
    char ** cmdAndArgsArr;
    printf(" @@@ The categories of command which can be executed :\n"
                   "A. Internal command\n"
                   "B. External command\n"
                   "C. External command < file\n"
                   "D. External command > file\n"
                   "E. External command in the background\n"
                   "F. Several external commands in piped mode\n"
                   "G. Quit the shell\n");
    while(true) {
        printf(" +++ Option => ");
        scanf(" %c", &opt);
        opt = toupper(opt);
        if (opt == 'G') {
            printf("Abort signal received. Quitting now ...\n");
            exit(0);
        }
        cout << " --> ";
        cin.ignore();
        getline(cin, cmd);
        printf("\n");
        switch(opt) {
            case 'A':
                execInternal (cmd);
                break;
            case 'B':
                execExternal (cmd, 0);
                break;
            case 'C':
                execExternal (cmd, 1);
                break;
            case 'D':
                execExternal (cmd, 2);
                break;
            case 'E':
                execExternal (cmd, 3);
                break;
            case 'F':
                pipeEmAll (cmd);
                break;
            default:
                printf(" *** Invalid option! :( Please try again!\n");
        }
    }
}